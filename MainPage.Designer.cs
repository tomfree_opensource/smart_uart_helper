﻿namespace 协议分析调试助手
{
    partial class MainPage
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDevice = new System.Windows.Forms.GroupBox();
            this.btnFresh = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.btnOpen2 = new System.Windows.Forms.Button();
            this.btnOpen1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbPortName2 = new System.Windows.Forms.ComboBox();
            this.cmbPortName1 = new System.Windows.Forms.ComboBox();
            this.txbInfo = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFontSetup = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txbLog = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbOverTime = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grpDevice.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOverTime)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDevice
            // 
            this.grpDevice.Controls.Add(this.btnFresh);
            this.grpDevice.Controls.Add(this.label3);
            this.grpDevice.Controls.Add(this.cmbBaudRate);
            this.grpDevice.Controls.Add(this.btnOpen2);
            this.grpDevice.Controls.Add(this.btnOpen1);
            this.grpDevice.Controls.Add(this.label2);
            this.grpDevice.Controls.Add(this.label1);
            this.grpDevice.Controls.Add(this.cmbPortName2);
            this.grpDevice.Controls.Add(this.cmbPortName1);
            this.grpDevice.Location = new System.Drawing.Point(12, 12);
            this.grpDevice.Name = "grpDevice";
            this.grpDevice.Size = new System.Drawing.Size(350, 105);
            this.grpDevice.TabIndex = 0;
            this.grpDevice.TabStop = false;
            this.grpDevice.Text = "设备";
            // 
            // btnFresh
            // 
            this.btnFresh.Location = new System.Drawing.Point(300, 76);
            this.btnFresh.Name = "btnFresh";
            this.btnFresh.Size = new System.Drawing.Size(41, 20);
            this.btnFresh.TabIndex = 6;
            this.btnFresh.Text = "刷新";
            this.btnFresh.UseVisualStyleBackColor = true;
            this.btnFresh.Click += new System.EventHandler(this.btnFresh_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "波特率：";
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Items.AddRange(new object[] {
            "9600",
            "57600",
            "115200"});
            this.cmbBaudRate.Location = new System.Drawing.Point(59, 76);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(107, 20);
            this.cmbBaudRate.TabIndex = 7;
            // 
            // btnOpen2
            // 
            this.btnOpen2.Location = new System.Drawing.Point(300, 50);
            this.btnOpen2.Name = "btnOpen2";
            this.btnOpen2.Size = new System.Drawing.Size(41, 20);
            this.btnOpen2.TabIndex = 5;
            this.btnOpen2.Text = "打开";
            this.btnOpen2.UseVisualStyleBackColor = true;
            this.btnOpen2.Click += new System.EventHandler(this.btnOpen2_Click);
            // 
            // btnOpen1
            // 
            this.btnOpen1.Location = new System.Drawing.Point(300, 24);
            this.btnOpen1.Name = "btnOpen1";
            this.btnOpen1.Size = new System.Drawing.Size(41, 20);
            this.btnOpen1.TabIndex = 4;
            this.btnOpen1.Text = "打开";
            this.btnOpen1.UseVisualStyleBackColor = true;
            this.btnOpen1.Click += new System.EventHandler(this.btnOpen1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "串口2：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "串口1：";
            // 
            // cmbPortName2
            // 
            this.cmbPortName2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPortName2.FormattingEnabled = true;
            this.cmbPortName2.Location = new System.Drawing.Point(59, 50);
            this.cmbPortName2.Name = "cmbPortName2";
            this.cmbPortName2.Size = new System.Drawing.Size(235, 20);
            this.cmbPortName2.TabIndex = 1;
            // 
            // cmbPortName1
            // 
            this.cmbPortName1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPortName1.FormattingEnabled = true;
            this.cmbPortName1.Location = new System.Drawing.Point(59, 24);
            this.cmbPortName1.Name = "cmbPortName1";
            this.cmbPortName1.Size = new System.Drawing.Size(235, 20);
            this.cmbPortName1.TabIndex = 0;
            // 
            // txbInfo
            // 
            this.txbInfo.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbInfo.Location = new System.Drawing.Point(6, 20);
            this.txbInfo.Name = "txbInfo";
            this.txbInfo.ReadOnly = true;
            this.txbInfo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.txbInfo.Size = new System.Drawing.Size(583, 498);
            this.txbInfo.TabIndex = 1;
            this.txbInfo.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFontSetup);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.txbInfo);
            this.groupBox1.Location = new System.Drawing.Point(368, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(595, 551);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "数据窗口";
            // 
            // btnFontSetup
            // 
            this.btnFontSetup.Location = new System.Drawing.Point(6, 522);
            this.btnFontSetup.Name = "btnFontSetup";
            this.btnFontSetup.Size = new System.Drawing.Size(69, 23);
            this.btnFontSetup.TabIndex = 3;
            this.btnFontSetup.Text = "字体设置";
            this.btnFontSetup.UseVisualStyleBackColor = true;
            this.btnFontSetup.Click += new System.EventHandler(this.btnFontSetup_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(525, 522);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(64, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "清除显示";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txbLog
            // 
            this.txbLog.Location = new System.Drawing.Point(6, 20);
            this.txbLog.Name = "txbLog";
            this.txbLog.ReadOnly = true;
            this.txbLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.txbLog.Size = new System.Drawing.Size(335, 298);
            this.txbLog.TabIndex = 3;
            this.txbLog.Text = "";
            this.txbLog.WordWrap = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txbLog);
            this.groupBox2.Location = new System.Drawing.Point(12, 239);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(350, 324);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "操作日志";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbOverTime);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(12, 123);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(350, 110);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "协议配置";
            // 
            // cmbOverTime
            // 
            this.cmbOverTime.Location = new System.Drawing.Point(61, 23);
            this.cmbOverTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.cmbOverTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cmbOverTime.Name = "cmbOverTime";
            this.cmbOverTime.Size = new System.Drawing.Size(51, 21);
            this.cmbOverTime.TabIndex = 9;
            this.cmbOverTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cmbOverTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.cmbOverTime.ValueChanged += new System.EventHandler(this.cmbOverTime_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(116, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "ms";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "超时时间";
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 575);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpDevice);
            this.MaximizeBox = false;
            this.Name = "MainPage";
            this.Text = "协议分析助手";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainPage_FormClosing);
            this.Load += new System.EventHandler(this.MainPage_Load);
            this.grpDevice.ResumeLayout(false);
            this.grpDevice.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOverTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDevice;
        private System.Windows.Forms.Button btnFresh;
        private System.Windows.Forms.Button btnOpen2;
        private System.Windows.Forms.Button btnOpen1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbPortName2;
        private System.Windows.Forms.ComboBox cmbPortName1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.RichTextBox txbInfo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox txbLog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFontSetup;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.NumericUpDown cmbOverTime;
    }
}

