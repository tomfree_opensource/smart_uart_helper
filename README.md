# 双串口智能协议分析助手

## 介绍

双串口智能协议分析助手，特性如下

1. 任意选择端口，带有端口设备类型描述
2. 任意选择波特率
3. 可设置超时时间
4. 带有操作日志
5. 数据双色显示
6. 带有时间标记
7. 可设置数据显示字体

## 主界面

![double_com_helper](./img/double_com_helper.png)

## 使用说明

1. git clone 当前仓库
2. VS2017以上版本打开
3. 编译运行

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
